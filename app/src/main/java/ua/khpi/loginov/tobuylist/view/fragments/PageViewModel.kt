package ua.khpi.loginov.tobuylist.view.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ua.khpi.loginov.tobuylist.model.ToBuyInstance
import ua.khpi.loginov.tobuylist.model.ToBuyInstanceDao

class PageViewModel : ViewModel() {

    lateinit var list: LiveData<List<ToBuyInstance>>

    var index: Int = 0

    fun init(index: Int, dao: ToBuyInstanceDao){
        this.index = index
        when(index){
            1 -> list = dao.getNotBought()
            2 -> list = dao.getBought()
        }
    }

}