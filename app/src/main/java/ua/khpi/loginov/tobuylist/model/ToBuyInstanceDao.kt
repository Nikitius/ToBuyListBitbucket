package ua.khpi.loginov.tobuylist.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ToBuyInstanceDao {

    @Query("SELECT * FROM to_buy_instance WHERE is_bought = 0 ORDER BY id DESC")
    fun getNotBought(): LiveData<List<ToBuyInstance>>

    @Query("SELECT * FROM to_buy_instance WHERE is_bought = 1 ORDER BY id DESC")
    fun getBought(): LiveData<List<ToBuyInstance>>

    @Insert
    fun insert(instance: ToBuyInstance)

    @Update
    fun update(instance: ToBuyInstance)

}