package ua.khpi.loginov.tobuylist.di.components

import android.app.Application
import android.content.Context
import dagger.Component
import ua.khpi.loginov.tobuylist.MyApplication
import ua.khpi.loginov.tobuylist.di.annotations.ApplicationContext
import ua.khpi.loginov.tobuylist.di.modules.ApplicationModule
import ua.khpi.loginov.tobuylist.di.annotations.DatabaseName
import ua.khpi.loginov.tobuylist.di.modules.MyDatabaseModule
import ua.khpi.loginov.tobuylist.view.activities.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [MyDatabaseModule::class, ApplicationModule::class])
interface ApplicationComponent {
    fun inject(application: MyApplication)
    fun inject(activity: MainActivity)

    @ApplicationContext
    fun getContext(): Context

    fun getApplication(): Application

    @DatabaseName
    fun getDatabaseName(): String
}