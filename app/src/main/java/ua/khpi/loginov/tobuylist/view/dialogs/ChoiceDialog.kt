package ua.khpi.loginov.tobuylist.view.dialogs

import android.Manifest
import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import ua.khpi.loginov.tobuylist.BuildConfig
import ua.khpi.loginov.tobuylist.R
import java.io.File

class ChoiceDialog(
    private val typeInDialog: TypeInDialog,
    private val sfm: FragmentManager,
    private var returnUri: ArrayList<Uri>
) : DialogFragment() {

    companion object {
        const val GET_FROM_CAMERA_REQUEST = 1
        const val GET_FROM_GALLERY_REQUEST = 2
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder
            .setTitle("Type in, take a picture or pick a picture from the gallery")
            .setItems(R.array.choice_options) { _: DialogInterface, i: Int ->
                when(i){
                    0 -> {typeInDialog.show(sfm, "typeInDialog")}
                    1 -> {
                        if(ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED){
                            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

                            val directory = File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES),
                                "ToBuyListPicks"
                            )

                            if(!directory.exists())
                                directory.mkdirs()

                            val file = File(
                                directory.path + "/photo_" + System.currentTimeMillis() + ".jpg"
                            )

                            val uri = FileProvider.getUriForFile(
                                requireContext(),
                                BuildConfig.APPLICATION_ID + ".provider",
                                file
                            )
                            returnUri.add(uri)

                            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                            requireActivity().startActivityForResult(intent,
                                GET_FROM_CAMERA_REQUEST
                            )
                        } else{
                            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
                        }
                    }
                    2 -> {
                        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                        intent.type = "image/*"
                        requireActivity().startActivityForResult(intent,
                            GET_FROM_GALLERY_REQUEST
                        )
                    }
                }
            }
            .setNegativeButton("Cancel") { _: DialogInterface, _: Int -> }
        return builder.create()
    }
}