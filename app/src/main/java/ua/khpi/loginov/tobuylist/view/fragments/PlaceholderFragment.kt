package ua.khpi.loginov.tobuylist.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ua.khpi.loginov.tobuylist.R
import ua.khpi.loginov.tobuylist.model.ToBuyInstance
import ua.khpi.loginov.tobuylist.model.ToBuyInstanceAdapter
import ua.khpi.loginov.tobuylist.view.activities.MainActivity
import java.util.concurrent.Executors


/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel

    private lateinit var listView: ListView

    private lateinit var header: View

    private val selectedItems: ArrayList<ToBuyInstance> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this)
            .get(PageViewModel::class.java)
            .apply {
            init(
                arguments?.getInt(ARG_SECTION_NUMBER) ?: 1,
                (requireActivity() as MainActivity).database.toBuyInstanceDao()
            )
        }
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        listView = root.findViewById(R.id.list)
        var adapter: ToBuyInstanceAdapter

        when(pageViewModel.index) {
            1 -> {
                header = createHeader()

                adapter = ToBuyInstanceAdapter(requireContext(), R.layout.to_buy_item, ArrayList())
                listView.adapter = adapter

                listView.onItemClickListener = AdapterView.OnItemClickListener{
                        _: AdapterView<*>, view1: View, _: Int, _: Long ->
                        val item = view1.getTag(R.integer.additional_tag_key) as ToBuyInstance
                        val checkBox: CheckBox = view1.findViewById(R.id.checkBox)
                        when(checkBox.isChecked){
                            true -> {
                                selectedItems.remove(item)
                                checkBox.isChecked = false
                            }
                            false -> {
                                selectedItems.add(item)
                                checkBox.isChecked = true
                            }
                        }

                        if(selectedItems.isEmpty()){
                            listView.removeHeaderView(header)
                            (requireActivity() as MainActivity).fab.visibility = View.VISIBLE
                        } else{
                            (requireActivity() as MainActivity).fab.visibility = View.GONE
                            if(listView.headerViewsCount == 0)
                                listView.addHeaderView(header)
                        }
                }
            }
            2 -> {
                adapter = ToBuyInstanceAdapter(requireContext(), R.layout.bought_item, ArrayList())
                listView.adapter = adapter
            }
        }

        adapter = listView.adapter as ToBuyInstanceAdapter

        pageViewModel.list.observe(this, Observer<List<ToBuyInstance>>{
            adapter.clear()
            adapter.addAll(it)
            adapter.notifyDataSetChanged()
        })

        return root
    }

    @SuppressLint("InflateParams")
    private fun createHeader(): View{
        val header: View = LayoutInflater.from(requireContext()).inflate(R.layout.list_header, null)
        header.findViewById<ImageButton>(R.id.checkAllButton).setOnClickListener{
            var allChecked = true
            for(i in 0 until listView.adapter.count){
                val view = if(listView.getChildAt(i) === null)
                    listView.adapter.getView(i, null, listView)
                else
                    listView.getChildAt(i)
                if(view !is ConstraintLayout)
                    if(!view.requireViewById<CheckBox>(R.id.checkBox).isChecked){
                        allChecked = false
                        break
                    }
            }

            if (allChecked){
                for(i in 0 until listView.adapter.count) {
                    val view = if(listView.getChildAt(i) === null)
                        listView.adapter.getView(i, null, listView)
                    else
                        listView.getChildAt(i)
                    if(view !is ConstraintLayout)
                        view.requireViewById<CheckBox>(R.id.checkBox).isChecked = false
                }
                selectedItems.clear()
                listView.removeHeaderView(header)
                (requireActivity() as MainActivity).fab.visibility = View.VISIBLE
            } else{
                for(i in 0 until listView.adapter.count){
                    val view = if(listView.getChildAt(i) === null)
                        listView.adapter.getView(i, null, listView)
                    else
                        listView.getChildAt(i)
                    if(view !is ConstraintLayout)
                        view.requireViewById<CheckBox>(R.id.checkBox).isChecked = true
                }
                for(item: ToBuyInstance in ((listView.adapter as HeaderViewListAdapter).wrappedAdapter as ToBuyInstanceAdapter).items)
                    selectedItems.add(item)
            }
        }

        header.findViewById<ImageButton>(R.id.confirmButton).setOnClickListener{
            listView.removeHeaderView(header)
            (requireActivity() as MainActivity).fab.visibility = View.VISIBLE

            val dao = (requireActivity() as MainActivity).dao
            for(item: ToBuyInstance in selectedItems){
                item.isBought = true
                Executors.newSingleThreadExecutor().execute{
                    dao.update(item)
                }
            }
            selectedItems.clear()
        }

        return header
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}