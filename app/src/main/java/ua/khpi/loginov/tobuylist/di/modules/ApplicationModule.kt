package ua.khpi.loginov.tobuylist.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import ua.khpi.loginov.tobuylist.di.annotations.ApplicationContext
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(): Context{
        return application
    }

    @Provides
    @Singleton
    fun provideApplication(): Application{
        return application
    }
}