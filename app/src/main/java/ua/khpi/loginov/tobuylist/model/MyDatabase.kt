package ua.khpi.loginov.tobuylist.model

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ToBuyInstance::class], version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun toBuyInstanceDao(): ToBuyInstanceDao

}