package ua.khpi.loginov.tobuylist.view.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import ua.khpi.loginov.tobuylist.MyApplication
import ua.khpi.loginov.tobuylist.R
import ua.khpi.loginov.tobuylist.model.MyDatabase
import ua.khpi.loginov.tobuylist.model.ToBuyInstance
import ua.khpi.loginov.tobuylist.model.ToBuyInstanceDao
import ua.khpi.loginov.tobuylist.view.dialogs.ChoiceDialog
import ua.khpi.loginov.tobuylist.view.fragments.SectionsPagerAdapter
import ua.khpi.loginov.tobuylist.view.dialogs.TypeInDialog
import java.util.concurrent.Executors
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var database: MyDatabase

    lateinit var dao: ToBuyInstanceDao

    private var imageUri: ArrayList<Uri> = ArrayList()

    lateinit var fab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fab = findViewById(R.id.fab)
        val sectionsPagerAdapter =
            SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        (application as MyApplication).getComponent().inject(this)
        dao = database.toBuyInstanceDao()
    }

    fun onFABClick(@Suppress("UNUSED_PARAMETER") view: View){
        val dialog = ChoiceDialog(
            TypeInDialog(dao),
            supportFragmentManager,
            imageUri
        )
        dialog.show(supportFragmentManager, "choiceDialog")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            when(requestCode) {
                ChoiceDialog.GET_FROM_CAMERA_REQUEST ->{
                        Executors.newSingleThreadExecutor().execute {
                            dao.insert(ToBuyInstance(0, null, imageUri[0].toString(), false))
                        }
                }
                ChoiceDialog.GET_FROM_GALLERY_REQUEST ->{
                    if(data !== null) {
                        val uri: Uri? = data.data
                        Executors.newSingleThreadExecutor().execute {
                            dao.insert(ToBuyInstance(0, null, uri.toString(), false))
                        }
                    }
                }
            }
    }
}