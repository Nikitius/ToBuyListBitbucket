package ua.khpi.loginov.tobuylist.view.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import ua.khpi.loginov.tobuylist.R
import ua.khpi.loginov.tobuylist.model.ToBuyInstance
import ua.khpi.loginov.tobuylist.model.ToBuyInstanceDao
import java.util.concurrent.Executors

class TypeInDialog(private val dao: ToBuyInstanceDao) : DialogFragment() {

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_type_in, null)
        val textInput: EditText = view.findViewById(R.id.textInput)
        val characterCounter: TextView = view.findViewById(R.id.characterCounter)
        textInput.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                characterCounter.text = textInput.text.length.toString() + " / 40"
            }

        })

        val builder = AlertDialog.Builder(activity)
        builder
            .setTitle("Type in your text")
            .setView(view)
            .setPositiveButton("Ok") { _: DialogInterface, _: Int ->
                Executors.newSingleThreadExecutor().execute {
                    dao.insert(ToBuyInstance(
                        0,
                        textInput.text.toString(),
                        null,
                        false))
                }
            }
            .setNegativeButton("Cancel") { _: DialogInterface, _: Int -> }
        return builder.create()
    }
}
