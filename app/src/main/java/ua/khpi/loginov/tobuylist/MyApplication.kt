package ua.khpi.loginov.tobuylist

import android.app.Application
import ua.khpi.loginov.tobuylist.di.components.ApplicationComponent
import ua.khpi.loginov.tobuylist.di.modules.ApplicationModule
import ua.khpi.loginov.tobuylist.di.components.DaggerApplicationComponent
import ua.khpi.loginov.tobuylist.di.modules.MyDatabaseModule

class MyApplication : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .myDatabaseModule(MyDatabaseModule(this))
            .build()
        applicationComponent.inject(this)
    }

    fun getComponent(): ApplicationComponent {
        return applicationComponent
    }

}